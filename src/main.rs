use std::fs::File;
use std::io::prelude::*;
use std::str::FromStr;

#[derive(Debug, Default, PartialEq, Eq)]
struct Draw {
    red: i32,
    green: i32,
    blue: i32,
}

impl Draw {
    fn parse_from_draw(draw: &str) -> Self {
        let mut out = Draw::default();
        for by_color in draw.split(",") {
            match by_color.trim().split_once(" ").unwrap() {
                (quantity, "red") => out.red += i32::from_str(quantity).unwrap(),
                (quantity, "green") => out.green += i32::from_str(quantity).unwrap(),
                (quantity, "blue") => out.blue += i32::from_str(quantity).unwrap(),
                x => println!("{:?}", x),
            }
        }
        out
    }

    fn is_possible(&self) -> bool {
        self.red <= 12 && self.green <= 13 && self.blue <= 14
    }
}

fn day2_part_1(input: &str) -> i32 {
    input
        .lines()
        .filter_map(|line| {
            let (game, value) = line.split_once(":").unwrap();
            let (_, game) = game.trim().split_once(" ").unwrap();
            let game = i32::from_str(game).unwrap();

            let mut draws = value.split(";").map(|draw| Draw::parse_from_draw(draw));
            draws
                .all(|draw| draw.is_possible())
                .then(||(game))
        })
        .sum()
}

fn day2_part_2(input: &str) -> i32 {
    input
        .lines()
        .filter_map(|line| {
            let (_game, value) = line.split_once(":").unwrap();

            value
                .split(";")
                .map(|draw| Draw::parse_from_draw(draw))
                .reduce(|acc, draw| Draw {
                    red: acc.red.max(draw.red),
                    green: acc.green.max(draw.green),
                    blue: acc.blue.max(draw.blue),
                })
        })
        .map(|draw| draw.red * draw.green * draw.blue)
        .sum()
}

fn main() -> std::io::Result<()> {

    let mut file = File::open("src/input.txt")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    println!("part 1: {}", day2_part_1(&contents));
    println!("part 2: {}", day2_part_2(&contents));

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    const INPUT: &str = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green
Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue
Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red
Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red
Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green";

    #[test]
    fn part_one() {
        assert_eq!(day2_part_1(INPUT), 8);
    }

    #[test]
    fn part_two() {
        assert_eq!(day2_part_2(INPUT), 2286);
    }
}
